"""Contains some example functions."""


def add_numbers(first: int, second: int) -> int:
    """Adds two integers.

    :param first: The first integer.
    :param second: The second integer.
    :returns: The sum of the first and second integer.
    """
    return first + second
