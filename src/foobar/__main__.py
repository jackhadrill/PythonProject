"""Entrypoint for application."""

from foobar.example import add_numbers


def main() -> None:
    """Prints example strings to stdout."""
    print("Hello, World!")
    print(add_numbers(40, 2))


if __name__ == "__main__":
    main()
