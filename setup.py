#!/usr/bin/env python3

# N.B. This is here for legacy support in order to support editable installs. Please see:
# https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html#pep660-status
# https://setuptools.pypa.io/en/latest/userguide/pyproject_config.html#setupcfg-caveats

from setuptools import setup


setup()
