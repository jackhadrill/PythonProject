# Python Template

This is a basic Python template which uses the new `pyproject.toml` in
conjunction with `setup.cfg` (and legacy support for older Python versions with
`setup.py`).

It contains pylint and flake8 rules for an increased line length of 120
characters, in addition to support for unit tests and CI via Drone.

## Development

The package should be located in a subdirectory within `src/`. For example,
`src/foobar/`.

This directory should contain an `__init__.py` and a `__main__.py`, the latter
of which allows this package to be called as a Python module. 

To install an editable version (such that you don't need to keep
re-installing), create a new virtual environment and use pip's editable flag.

```bash
$ python -m venv venv
$ . venv/bin/activate
(venv)$ pip install -e .
```

### Lint

Pylint and flake8 rules can be found in `.pylintrc` and `.flake8` respectively.
By default, the default rulesets are followed with overrides specified in these
files.

To install, use the following command:
```bash
(venv)$ pip install -e '.[lint]'
```

To run, use the following commands:
```bash
(venv)$ flake8 src
(venv)$ pylint src
```

It should be noted that flake8 is much quicker than pylint, so should run first
when used in a CI pipeline.

### Test

This package is setup to support Python unittest and pytest, although in
practice I've only really used it with pytest.

To install the test dependencies, issue the following command:
```bash
(venv)$ pip install -e '.[test]'
```

To run the tests, run the following command:
```bash
(venv)$ pytest
```

Tests should be stored in the `test/` directory.

### Build

To build a wheel for this package, use the following commands:
```bash
(venv)$ pip install --no-cache-dir build
(venv)$ python -m build --wheel
```

This will output a wheel to the `dist/` directory.

## Installation

Installation can be performed from source or using the previously built wheel.

### Source

To install from source, navigate to the root directory and run the following
command:
```bash
(venv)$ pip install .
```

### Wheel

To install using the wheel, run the following command:
```bash
(venv)$ pip install dist/foobar*.whl
```

## Running

The `[project.scripts]` directive in `pyproject.toml` lists the commands which
should be installed as part of this package. The included example runs the
`main()` function in `__main__.py`, and allows for the user to run the package
with the following command:
```bash
(venv)$ foobar
Hello, World!
42
```

Alternatively, because `__main__.py` was used, the user can achieve the same by
using the following:
```bash
(venv)$ python -m foobar
Hello, World!
42
```
