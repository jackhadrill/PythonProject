"""Tests for example.py."""

from foobar.example import add_numbers


def test_add() -> None:
    """Assert that two integers add together correctly."""
    # Arrange
    first = 40
    second = 2
    expected = 42

    # Act
    ret = add_numbers(first, second)

    # Assert
    assert ret == expected
